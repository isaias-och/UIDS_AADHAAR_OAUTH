﻿using Api.Entidades;
using Api.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Servicios
{
    public class EntidadesServicio
    {
        private readonly EntidadesRepositorio repo;

        public EntidadesServicio()
        {
            repo = new EntidadesRepositorio();
        }

        public IEnumerable<Entidad> ObtenerTodos()
        {
            return repo.ObtenerTodos();
        }

    }
}