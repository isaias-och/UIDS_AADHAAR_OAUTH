﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class EntidadesRepositorio
    {

        private readonly BaseDeDatos db;


        public EntidadesRepositorio()
        {
            db = new BaseDeDatos();
        }

        public IEnumerable<Entidad> ObtenerTodos()
        {
            return db.Entidades.ToList();


        }


    }
}