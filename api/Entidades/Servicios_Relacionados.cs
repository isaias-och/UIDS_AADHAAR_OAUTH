﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("REG_SERVICIO")]
    public class Servicios_Relacionados
    {
        [Key]
        public int ID_SERVICIO { get; set; }
        public string DESCRIPCION { get; set; }
    }
}