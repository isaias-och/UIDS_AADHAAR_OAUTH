﻿using Api.Entidades;
using Api.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class UsuariosController : ApiController
    {
        private readonly UsuariosServicio usuarios;

        public UsuariosController()
        {
            usuarios = new UsuariosServicio();

        }
        // [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(usuarios.ObtenerTodos());

        }

        public IHttpActionResult Post(Usuario usuario)
        {
            return Ok(usuarios.Crear(usuario));
        }

        public IHttpActionResult Put(Usuario usuario)
        {
            return Ok(usuarios.Editar(usuario));
        }


    }
}