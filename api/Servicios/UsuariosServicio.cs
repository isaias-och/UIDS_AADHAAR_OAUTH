﻿using Api.Entidades;
using Api.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Servicios
{
    public class UsuariosServicio
    {
        private readonly UsuariosRepositorio repo;

        public UsuariosServicio()
        {
            repo = new UsuariosRepositorio();
        }

        public IEnumerable<Usuario> ObtenerTodos()
        {
            return repo.ObtenerTodos();

        }

        public int ContarTodos()
        {
            return repo.ContarTodos();
        }

        public Usuario BuscarPorId(int id)
        {
            return repo.BuscarPorId(id);
        }

        public int Crear(Usuario usuario)
        {
            return repo.Crear(usuario);

        }

        public bool Editar(Usuario usuario)
        {

            return repo.Editar(usuario);
        }


    }
}