﻿using Api.Entidades;
using Api.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Servicios
{
    internal class TiposUsuariosServicios
    {
        private readonly TiposUsuariosRepositorios repo;

        public TiposUsuariosServicios()
        {
            repo = new TiposUsuariosRepositorios();
        }

        public IEnumerable<TipoUsuario> ObtenerTodos()
        {

            return repo.ObtenerTodos();
        }


        

    }
}