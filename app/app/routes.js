﻿(function () {
    angular.module("app").config(configRoutes);

    function configRoutes($urlRouterProvider, $stateProvider) {
        $urlRouterProvider
            .when("",  "/home/dashboard")
            .when("/", "/home/dashboard");

        $stateProvider
            .state("default", {
                abstract: true,
                url: "",
                controller: "LayoutController",
                controllerAs: "vm",
                views: {
                    layout: {
                        templateUrl: "app/core/layout.template.html"
                    }
                }
            })
            .state("home", {
                abstract: true,
                url: "/home",
                template: "<div ui-view />",
                parent: "default"
            })
                .state("home.dashboard", {
                    url: "/dashboard",
                    templateUrl: "app/home/dashboard.template.html"
                })
            .state("mantenimientos", {
                abstract: true,
                url: "/mantenimientos",
                template: "<div ui-view />",
                parent: "default"
            })
                .state("mantenimientos.productos", {
                    url: "/productos",
                    controller: "ProductosListadoController",
                    controllerAs: "vm",
                    templateUrl: "app/mantenimientos/productos-listado.template.html"
                })
                .state("mantenimientos.productos-crear", {
                    url: "/productos/crear",
                    controller: "ProductosCrearController",
                    controllerAs: "vm",
                    templateUrl: "app/mantenimientos/productos-crear.template.html"
                })
                .state("mantenimientos.usuarios", {
                    url: "/usuarios",
                    controller: "UsuariosListadoController",
                    controllerAs: "vm",
                    templateUrl: "app/mantenimientos/usuarios-listado.template.html"
                })
                .state("mantenimientos.usuarios-crear", {
                    url: "/usuarios/crear",
                    controller: "UsuariosCrearController",
                    controllerAs: "vm",
                    templateUrl: "app/mantenimientos/usuarios-crear.template.html"
                })
                .state("personas", {
                abstract: true,
                url: "/personas",
                template: "<div ui-view />",
                parent: "default"
            })
                .state("personas.registro", {
                    url: "/registro",
                    controller: "RegistrosListadoController",
                    controllerAs: "vm",
                    templateUrl: "app/registro/registro-listado.template.html"
                })
                .state("personas.registro-crear", {
                    url: "/registro/crear",
                    controller: "RegistroCrearController",
                    controllerAs: "vm",
                    templateUrl: "app/registro/registro-crear.template.html"
                })
            .state("reportes", {
                abstract: true,
                url: "/reportes",
                template: "<div ui-view />",
                parent: "default"
            })
                    .state("reportes.index", {
                        url: "/reportes",
                        controller: "ReporteIndexController",
                        controllerAs: "vm",
                        templateUrl: "app/reporteria/reporteria-index.template.html"
                    })

        ;

    }
})();