﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("REG_PERSONA")]
    public class Persona
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AADHAAR { get; set; }
        public int AADHAAR_PADRE { get; set; }
        public int AADHAAR_MADRE { get; set; }
        public int ID_USUARIO { get; set; }
        public string ID_PERSONAL { get; set; }
        [StringLength(200)]
        public string NOMBRE { get; set; }
        [StringLength(200)]
        public string APELLIDOS { get; set; }
        [StringLength(250)]
        public string DIRECCION { get; set; }

        public int ID_CIUDAD_RESIDENCIA { get; set; }

        public bool GENERO { get; set; }

        public DateTime FECHA_REGISTRO { get; set; }
        public DateTime FECHA_NACIMIENTO { get; set; }

        public int ID_CIUDAD_NACIMIENTO { get; set; }

        public int ID_ESTADO_CIVIL { get; set; }

        public DateTime FECHA_VENCIMIENTO { get; set; }

        public DateTime FECHA_DEFUNCION { get; set; }

        public string URL_FOTO { get; set; }
        public string URL_HUELLAS { get; set; }

        public string URL_IRIS { get; set; }

    }
}