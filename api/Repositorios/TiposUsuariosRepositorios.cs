﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class TiposUsuariosRepositorios
    {
        private readonly BaseDeDatos db;


        public TiposUsuariosRepositorios()
        {
            db = new BaseDeDatos();
        }

        public IEnumerable<TipoUsuario> ObtenerTodos()
        {
            return db.TiposUsuarios.ToList();


        }


    }
}