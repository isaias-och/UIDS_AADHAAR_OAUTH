﻿using Api.Entidades;
using Api.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Servicios
{
    public class PersonasServicio
    {
        private readonly PersonasRepositorio repo;

        public PersonasServicio()
        {
            repo = new PersonasRepositorio();
        }

        public IEnumerable<Persona> obtenerTodas()
        {
            return repo.ObtenerTodas();
        }

        public int ContarTodos()
        {
            return repo.ContarTodos();
        }

        public Persona BuscarPorId(int id)
        {
            return repo.BuscarPorId(id);
        }

        public int Crear(Persona persona)
        {
            return repo.Crear(persona);

        }

        public bool Editar(Persona persona)
        {

            return repo.Editar(persona);
        }

    }
}