﻿(function () {

    angular.module("app").controller("UsuariosCrearController", UsuariosCrearController);

    function UsuariosCrearController(usuariosServicio, entidadesServicio, $scope) {
        $scope.vm = this;
        var vm = this;
        vm.entidades = [];
        vm.entidad = null;
        vm.tiposUsuarios = [];
        vm.tipoUsuario = null;

        
        vm.nombre = null;
        vm.usuario = null;
        vm.password = null;


        init();

        function init() {

            cargarEntidades();
            cargartiposUsuarios();
        }

        function cargartiposUsuarios() {

            usuariosServicio.obtenerTodosTipos().then(cargadosTiposUsuarios);
        }

        function cargadosTiposUsuarios(tipoUsuario) {
            console.log(tipoUsuario);
            vm.tiposUsuarios = tipoUsuario;

        }

        function cargarEntidades() {

            entidadesServicio.obtenerTodos().then(cargar);

        }

        function cargar(entidades) {

            console.log(entidades);
            vm.entidades = entidades;
        }

        vm.guardar = function () {

            console.log("entidad ", vm.entidad);
            console.log("tipoUsuario ", vm.tipoUsuario);
            var usuario = {
                ID_USUARIO : 1,
                ID_ENTIDAD: vm.entidad,
                ID_TIPO_USUARIO: vm.tipoUsuario,
                nombre: vm.nombre,
                correo: vm.usuario,
                password : vm.password
            }

            console.log("usuario ", usuario);

            usuariosServicio.crearUsuario(usuario).then(UsuarioCreado);

            //usuariosServicio.crearUsuario(usuario).then(UsuarioCreado);
            
        }

        function UsuarioCreado(usuario) {

            console.log("creado usuario", usuario);
        }

    }

})();