﻿(function () {

	angular.module("app").controller("UsuariosListadoController", UsuariosListadoController);

    function UsuariosListadoController(usuariosServicio) {

        var vm = this;

        vm.pagina = 1;
        vm.usuarios = [];
        vm.usuario = null;
        init();

        function init() {
            obtenerProductos();
        }

        function obtenerProductos() {
            usuariosServicio.obtenerTodos(vm.pagina).then(cargar);
        }

        function cargar(usuarios) {
            console.log(usuarios);
            vm.usuarios = usuarios;
        }
    }

})();