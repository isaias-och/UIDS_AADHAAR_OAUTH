﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class UsuariosRepositorio
    {
        private readonly BaseDeDatos db;

        public UsuariosRepositorio()
        {
            db = new BaseDeDatos();
        }

        public IEnumerable<Usuario> ObtenerTodos()
        {
            return db.Usuarios.ToList();

        }

        public int ContarTodos()
        {
            return db.Usuarios.Count();
        }
        public Usuario BuscarPorId(int id)
        {
            return db.Usuarios.SingleOrDefault(x => x.ID_USUARIO == id);
        }
        public int Crear(Usuario usuario)
        {
            db.Usuarios.Add(usuario);
            db.SaveChanges();
            return usuario.ID_USUARIO;
        }

        public bool Editar(Usuario usuario)
        {
            db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges() > 0;
        }


    }
}