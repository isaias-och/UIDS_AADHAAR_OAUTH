﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("PERSONA_SERVICIO")]
    public class Persona_Servicio
    {
        [Key]
        public int AADHAAR { get; set; }
        public int ID_SERVICIO { get; set; }
        public DateTime FECHA_REGISTRO { get; set; }
        public DateTime FECHA_ACTIVIDAD { get; set; }

    }
}