﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("CAT_CIUDAD")]
    public class Ciudad
    {

            [Key]
            public int ID_CIUDAD { get; set; }
            public int ID_ESTADO { get; set; }
            [StringLength(50)]
            public string NOMBRE { get; set; }
        
    }
}