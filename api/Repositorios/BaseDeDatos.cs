﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class BaseDeDatos : DbContext
    {
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<FacturaDetalle> FacturaDetalles { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Persona_Servicio> Personas_Servicios { get; set; }
        public DbSet<Ciudad> Ciudades { get; set; }
        public DbSet<Entidad> Entidades { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Estado_Persona> Estados_Personas { get; set; }
        public DbSet<Servicios_Relacionados> Servicios_Relacionados { get; set; }
        public DbSet<TipoUsuario> TiposUsuarios { get; set; }


        static BaseDeDatos()
        {
            // Esto para que Entity Framework nunca intente crear la base de datos.
            // se asume que la base de datos ya existirá.
            Database.SetInitializer(new NullDatabaseInitializer<BaseDeDatos>());
        }

        public BaseDeDatos() : base("CadenaDeConexion")
        {
        }
    }
}