﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("CAT_ESTADOS_PERSONA")]
    public class Estado_Persona
    {
        [Key]
        public int ID_ESTADO_CIVIL { get; set; }
        public string DESCRIPCION { get; set; }

    }
}