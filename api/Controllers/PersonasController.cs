﻿using Api.Entidades;
using Api.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class PersonasController : ApiController
    {

        private readonly PersonasServicio personas;

        public PersonasController()
        {
            personas = new PersonasServicio();

        }

        public IHttpActionResult Get()
        {
            return Ok(personas.obtenerTodas());
        }


        public IHttpActionResult Post(Persona persona)
        {
            return Ok(personas.Crear(persona));
        }

        public IHttpActionResult Put(Persona persona)
        {
            return Ok(personas.Editar(persona));
        }


    }
}