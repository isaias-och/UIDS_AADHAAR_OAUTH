﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class PersonasRepositorio
    {
        private readonly BaseDeDatos db;

        public PersonasRepositorio()
        {
            db = new BaseDeDatos();
        }

        public IEnumerable<Persona> ObtenerTodas()
        {
            return db.Personas.ToList();

        }

        public int ContarTodos()
        {
            return db.Personas.Count();
        }
        public Persona BuscarPorId(int id)
        {
            return db.Personas.SingleOrDefault(x => x.AADHAAR == id);
        }
        public int Crear(Persona persona)
        {
            db.Personas.Add(persona);
            db.SaveChanges();
            return persona.ID_USUARIO;
        }

        public bool Editar(Persona persona)
        {
            db.Entry(persona).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges() > 0;
        }

    }
}