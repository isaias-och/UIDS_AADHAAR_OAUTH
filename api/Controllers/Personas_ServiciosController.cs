﻿using Api.Entidades;
using Api.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class Personas_ServiciosController :ApiController
    {
        private readonly Persona_ServicioServicio personas_servicio;

        public Personas_ServiciosController()
        {
            personas_servicio = new Persona_ServicioServicio();

        }

        public IHttpActionResult Get()
        {
            return Ok(personas_servicio.obtenerTodas());
           
        }


        public IHttpActionResult Post(Persona_Servicio persona)
        {
            return Ok(personas_servicio.Crear(persona));
        }

        public IHttpActionResult Put(Persona_Servicio persona)
        {
            return Ok(personas_servicio.Editar(persona));
        }


    }
}