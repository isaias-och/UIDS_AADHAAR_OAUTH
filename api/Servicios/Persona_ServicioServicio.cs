﻿using Api.Entidades;
using Api.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Servicios
{
    public class Persona_ServicioServicio
    {
        private readonly Persona_ServicioRepo repo;

        public Persona_ServicioServicio()
        {
            repo = new Persona_ServicioRepo();
        }

        public IEnumerable<Persona_Servicio> obtenerTodas()
        {
            return repo.ObtenerTodas();
        }


        public int ContarTodos()
        {
            return repo.ContarTodos();
        }

        public Persona_Servicio BuscarPorId(int id)
        {
            return repo.BuscarPorId(id);
        }

        public int Crear(Persona_Servicio personaServicio)
        {
            return repo.Crear(personaServicio);

        }

        public bool Editar(Persona_Servicio personaServicio)
        {

            return repo.Editar(personaServicio);
        }

    }
}