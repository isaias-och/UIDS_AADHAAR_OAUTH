﻿using Api.Entidades;
using Api.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class EntidadesController : ApiController
    {
        private readonly EntidadesServicio servicio;

        public EntidadesController()
        {
            servicio = new EntidadesServicio();

        }

        public IHttpActionResult Get()
        {
            return Ok( servicio.ObtenerTodos());


        }

    }
}