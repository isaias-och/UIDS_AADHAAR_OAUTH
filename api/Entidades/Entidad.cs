﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("MNT_ENTIDAD")]

    public class Entidad
    {
            [Key]
            public int ID_ENTIDAD { get; set; }

            [StringLength(250)]
            public string NOMBRE { get; set; }

    }
}