﻿(function () {

    angular.module("app").controller("RegistrosListadoController", RegistrosListadoController);

    function RegistrosListadoController(registroServicio) {

        var vm = this;

        vm.pagina = 1;
        vm.personas = [];
        
        init();

        function init() {
            obtenerProductos();
        }
        

        function obtenerProductos() {
            registroServicio.obtenerTodos(vm.pagina).then(cargar);
        }

        function cargar(personas) {
            console.log(personas);
            //vm.productos = productos;
        }
    }

})();