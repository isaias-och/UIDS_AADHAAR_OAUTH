﻿(function () {
    angular.module("app").service("sidebarMenuService", SidebarMenuService);

    function SidebarMenuService($q) {
        var svc = this;

        svc.getSections = getSections;

        function getSections() {
            return $q.resolve([
                {
                    text: "General",
                    items: [
                        {
                            text: "Home",
                            icon: "home",
                            items: [
                                {
                                    text: "Dashboard",
                                    state: "home.dashboard"
                                }
                            ]
                        },
                        {
                            text: "Registro de Personas",
                            icon: "table",
                            items: [
                                {
                                    text: "Crear persona",
                                    state: "personas.registro"
                                },
                                {
                                    text: "Buscar persona",
                                    state: "mantenimientos.usuarios"
                                }
                            ]
                        },
                        {
                            text: "Mantenimientos",
                            icon: "edit",
                            items: [
                                {
                                    text: "Usuarios",
                                    state: "mantenimientos.usuarios"
                                }
                            ]
                        },
                        {
                            text: "Reportes",
                            icon: "bar-chart",
                            items: [
                                {
                                    text: "Listado de Reportes",
                                    state: "reportes.index"
                                }
                            ]
                        }
                    ]
                }
            ]);
        }
    }
})();