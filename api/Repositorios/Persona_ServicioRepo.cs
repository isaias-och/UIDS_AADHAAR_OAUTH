﻿using Api.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repositorios
{
    public class Persona_ServicioRepo
    {
        private readonly BaseDeDatos db;

        public Persona_ServicioRepo()
        {
            db = new BaseDeDatos();
        }

        public IEnumerable<Persona_Servicio> ObtenerTodas()
        {
            return db.Personas_Servicios.ToList();

        }

        public int ContarTodos()
        {
            return db.Personas_Servicios.Count();
        }
        public Persona_Servicio BuscarPorId(int id)
        {
            return db.Personas_Servicios.SingleOrDefault(x => x.AADHAAR == id);
        }
        public int Crear(Persona_Servicio personaServicio)
        {
            db.Personas_Servicios.Add(personaServicio);
            db.SaveChanges();
            return personaServicio.AADHAAR;
        }

        public bool Editar(Persona_Servicio personaServicio)
        {
            db.Entry(personaServicio).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges() > 0;
        }
    }
}