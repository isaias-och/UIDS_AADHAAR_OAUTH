﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("MNT_TIPO_USUARIO")]
    public class TipoUsuario
    {
        [Key]
        public int ID_TIPO_USUARIO { get; set; }

        [StringLength(250)]
        public string DESCRIPCION { get; set; }

    }
}