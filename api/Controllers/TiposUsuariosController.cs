﻿using Api.Entidades;
using Api.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class TiposUsuariosController : ApiController
    {
        private readonly TiposUsuariosServicios servicio;

        public TiposUsuariosController()
        {
            servicio = new TiposUsuariosServicios();
        }

        public IHttpActionResult Get()
        {
            return Ok(servicio.ObtenerTodos());

        }


    }
}