﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Entidades
{
    [Table("CAT_ESTADO")]
    public class Estado
    {
        [Key]
        public int ID_ESTADO { get; set; }
        [StringLength(50)]
        public string NOMBRE { get; set; }
    }
}